// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA1bmLM6OqA8qyMaBjwWjgPgvtTzX_m4yo",
    authDomain: "auth-63415.firebaseapp.com",
    projectId: "auth-63415",
    storageBucket: "auth-63415.appspot.com",
    messagingSenderId: "840521575041",
    appId: "1:840521575041:web:f197fd1f0dfcb302b3bb80",
    measurementId: "G-W3EJMP9Y9T",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
