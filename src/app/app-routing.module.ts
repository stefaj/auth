import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterViewComponent } from "./auth/register-view/register-view.component";
import { LoginViewComponent } from "./auth/login-view/login-view.component";

const routes: Routes = [
  {
    path: 'register',
    component: RegisterViewComponent,
  },
  {
    path: 'login',
    component: LoginViewComponent,
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
